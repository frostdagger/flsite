var app = (function () {

    return $.extend(app, {
        header: function () {
            var _this = this;

            this.$body = $('body');
            this.$searchToggle = $('#searchToggle');
            this.$searchInput = $('#searchInput');
            this.$subscribeButton = $('#subscribe');
            this.$subscribeModal = $('#subscribeModal');
            this.$closeModal = $('.close-modal');
            this.$overlayModal = $('.modal');
            this.$innerModal = $('.inner-modal');
            this.$moreButton = $('#more');
            this.$socialPopup = $('#socialPopup');
            this.$stickyPopup = $('.sticky-popup');
            this.$offcanvasToggle = $('.toggle-offcanvas');
            this.$offcanvas = $('.offcanvas');
            this.$pageBox = $('.page-box');
            this.$topicToggle = $('#toggleTopics');
            this.$dropdownTopics = $('.offcanvas-dropdown');
            this.$scrollTopArrow = $('.top-arrow');
            this.$invisibleItems = $('.invisible');
            this.$previewLink = $('.preview-link');

            /*Smooth appearance*/

            var SmoothReveal = {

                multiplier: 3,
                timeStep: 100,

                visibilityClass: 'visible',
                invisibilityClass: 'invisible',

                init: function (elements) {
                    SmoothReveal.revealTimer(elements);
                },

                elapsingTime: function () {
                    return this.multiplier * this.timeStep;
                },

                revealTimer: function (el) {

                    $.each(el, function () {
                        var timer = Number($(this).data('order')) * SmoothReveal.elapsingTime();
                        $(this).data('timer', timer);
                    });
                },

                smoothAppearance: function (el) {
                    el.removeClass(this.invisibilityClass)
                      .addClass(this.visibilityClass);
                }

            };

            SmoothReveal.init(_this.$invisibleItems);

            this.$invisibleItems.each(function () {
                var timeout = $(this).data('timer'),
                    el = $(this);

                setTimeout(function() {
                    SmoothReveal.smoothAppearance(el);
                }, timeout);
            });

            /*End of smooth appearance*/

            this.$searchToggle.on('click', function () {
                _this.$searchInput.toggleClass('show-input');
            });

            this.$subscribeButton.on('click', function () {
                _this.$subscribeModal.show();
            });

            this.$overlayModal.on('click', function () {
               _this.$overlayModal.hide();
            });

            this.$innerModal.on('click', function (e) {
                e.stopPropagation();
            });

            this.$closeModal.on('click', function () {
                $(this).parents('.modal').hide();
            });

            this.$scrollTopArrow.on('click', function () {
                $('body, html').animate({
                    scrollTop: "0px"
                }, 500);
            });

            $(window).on('scroll', function () {
               if ($(this).scrollTop() > 0) {
                   _this.$scrollTopArrow.show();
               } else {
                   _this.$scrollTopArrow.hide();
               }
            });

            /*End main site navigation and modals*/

            /*Offcanvas manipulations*/

            this.$offcanvasToggle.on('click', function () {
                _this.$offcanvas.toggleClass('show-offcanvas');
                _this.$pageBox.toggleClass('transformed-content');
            });

            this.$topicToggle.on('click', function () {
                _this.$dropdownTopics.toggle();
            });

            var touchstartXPos,
                touchendXPos;

            this.$body.on('touchstart touchmove touchend', function (e) {
                var movementReserveOpen = 70,
                    movementReserveClose = 50,
                    directionX,
                    event = e.type;

                if (event == 'touchstart') {
                    touchstartXPos = e.originalEvent.changedTouches[0].pageX;
                }

                if (event == 'touchmove') {
                    touchendXPos = e.originalEvent.changedTouches[0].pageX;
                }


                if (event == 'touchend') {
                    directionX = touchstartXPos - touchendXPos;

                    if (Math.abs(directionX) > movementReserveOpen) {
                        _this.$offcanvas.addClass('show-offcanvas');
                        _this.$pageBox.addClass('transformed-content');
                        stickPopupToTop($(window).scrollTop())
                    }
                    if (directionX > movementReserveClose) {
                        _this.$offcanvas.removeClass('show-offcanvas');
                        _this.$pageBox.removeClass('transformed-content');
                        stickPopupToTop($(window).scrollTop())
                    }

                }
            });

            /*End Offcanvas manipulation*/

            /*Sticky popup*/

            var StickyPopup = {

                scrollBarrier: 100,

                isOffcanvasVisible: function (offcanvas) {

                    if (offcanvas) {
                        return 'shown';
                    } else {
                        return 'hidden';
                    }
                },

                showPopup: function (scroll, offcanvas) {

                    if (scroll > this.scrollBarrier) {

                        if (this.isOffcanvasVisible(offcanvas) == 'shown') {
                            return 'hidePopup';
                        } else {
                            return 'showPopup'
                        }
                    } else {
                        return 'hidePopup'
                    }
                }
            };

            /*Endof sticky pop-up*/

            /*Sticky ad*/

            var $stickyAd = $('aside > .ad-block:last-of-type'),
                stickyWidth = $stickyAd.outerWidth(),
                stickyOffset = $stickyAd.offset().top;

            $(window).on('scroll', function () {
                var scrollHeight = $(this).scrollTop();

                stickAdToTop(scrollHeight);
                stickPopupToTop(scrollHeight)
            });

            function stickPopupToTop(scrollHeight) {

                var togglePopup = StickyPopup
                    .showPopup(scrollHeight, _this.$offcanvas.hasClass('show-offcanvas'));

                if (togglePopup == 'showPopup') {
                    _this.$stickyPopup.addClass('show-sticky-popup');
                } else {
                    _this.$stickyPopup.removeClass('show-sticky-popup');
                }
            }

            function stickAdToTop(scrollHeight) {

                var paddingMargin = 11;

                if (scrollHeight > stickyOffset + paddingMargin) {
                    $stickyAd.addClass('sticky-ad');
                    $stickyAd.css({'width': stickyWidth});
                } else {
                    $stickyAd.removeClass('sticky-ad');
                }
            }

            /*End of sticky ad*/

            /*Social popup-modal*/

            this.$moreButton.on('click', function () {
                _this.$socialPopup.show();
            });

            /*End of social popup-modal*/

            /*equal image=container heights*/

            // this.$previewLink.outerHeight($(_this.$previewLink).find('img').outerHeight());

            /*end of equal heights*/
        }
    })
})();